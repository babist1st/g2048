#! /usr/bin/python3

import random, copy, curses

def game_logic(l):
    oldHash = hash(tuple(l))

    line_active= True
    global moved
    merged = False
    while line_active:
        for i in range(board_line-1):
            if l[i] == None and l[i+1] != None:
                l.pop(i)
                l.insert(i+1,None)
            elif l[i] != None and l[i] == l[i+1] and merged != True:
                mi=l.pop(i)
                l.insert(i,int(mi)*2)

                l.pop(i+1)
                l.insert(i+1,None)

                merged = True
                oldHash = hash(tuple(l))
                moved = True

        newHash = hash(tuple(l))

        if oldHash == newHash:
            # print('Exiting: Boxes didn't move')
            line_active = False
        else:
            # print('Repeating: Boxes moved')
            oldHash = newHash
            moved = True

    return l
            
def movement():
    stdscr.addstr(10, 0, '\nMove the numbers: ')
    m = stdscr.getkey()
    global moved
    global board
    global board_reset
    moved=False
    board_temp=copy.deepcopy(board)

    undo = ['u','r']

    up = ['k','w'] #straight
    left = ['h','a'] #straight
    down = ['j','s'] #back
    right = ['l','d'] #back

    line = left + right
    column = up + down

    def rev(rl):
        if m in left + up:
            gl = game_logic(rl) #k
        else:
            gl = game_logic(rl[::-1])[::-1] #j
        return gl

    if m in line:
        for i in range(0,board_size,board_line):
            rl = board[i:i+board_line]
            board[i:i+board_line] = rev(rl)
    elif m in column:
        for i in range(board_line):
            rl = board[i::board_line]
            board[i::board_line] = rev(rl)
    elif m in undo:
        board=board_reset
    else:
        raise ValueError('Can not move this way')

    if m not in undo and moved == True:
        board_reset=copy.deepcopy(board_temp)


def print_board():
    for i in range(0,board_size,board_line):
        stdscr.addstr(i, 0, '{!s:<4}'.join(' '*(board_line+1)).format(*board[i:i+board_line]))
    stdscr.refresh()

def add_number():
    try:
        global board
        global li
        indices = []
        indices = [i for i, x in enumerate(board) if x == None]
        li=len(indices)
        p = random.choice(indices)
        board[p] = random.choices([2,4],weights=[80,20])[0]
        stdscr.addstr(13, 0, '\nNew number at position '+str(p+1))
        stdscr.addstr(15,0,str(li)+str(p)+str(indices)+'                 ')
    except e:
        print(e)

def run(*args):
    #TODO: Make a class so i can remove globals and simplify code
    # class Game():
    #   def init():
    global li
    global board
    global board_reset
    game_state = True
    board = []
    global board_size
    global board_line
    board_line = 2
    board_size = board_line**2
    for i in range(board_size):
        board.insert(i,None)

    global stdscr
    stdscr = curses.initscr()
    add_number()
    board_reset = copy.deepcopy(board)

    global moved
    while game_state:

        print_board()

        try:
            movement()
        except ValueError:
            # continue
            pass

        if moved == False and li == 0:
            #game_state = False
            stdscr.addstr(15, 0, 'Game Over')
            #print('Game Over')
            #curses.endwin()
            return
        stdscr.addstr(13, 0, 'MOVED          ')

        if moved == False:
            stdscr.addstr(13, 0, 'DID NOT MOVE')
            continue

        try:
            add_number()
        except IndexError:
            # catch game over here
            game_state = False
            print('Game Over')

curses.wrapper(run)

state = True
while state:
    m = stdscr.getkey()
    if m == 'q':
        state = False
        curses.endwin()
